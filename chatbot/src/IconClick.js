import React, { Component, useState,useContext } from 'react';
import ChatWindow  from './ChatContext';
// import ChatScreen from './ChatScreen';
import './Css2/Chat.css';
import './css/ChatTheme.scss';


const IconClick = ()=>  {
	
	// Here, we use useContext since we need to first fetch the context. Once fetched, use setWindow to change its state on global level.
	const [window, setWindow] = useContext(ChatWindow) ;

	function handleClick() {		
		console.log(window);	
		setWindow(!window);
	} 

	 return (
				<div className="IcnOtr">
				  <div className="IcnClk">
					<img src={require("./images/woman.png")} className="IgSz" style={{boxShadow: '0px 0px 12px #a4a4a4', borderRadius: '100%'}} />
					<div className="ChtWndOtr Window-Home">
					  <div className="TpHdr Header-Bg">
						<div className="Hdr Header-Text-Color">Chat Bot</div>
						<div className="Clr" />
						<div className="BblOtr-01">
						  <div style={{padding: '15px'}}>
							<div className="HiTxt">Hi There...!</div>
							<div className="HiSub">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</div>
							<div className="HmBlkOtr-01 Home-Links-Bg">
							  <div className="BlkLnkOtr Home-Links-Text"><a href="#">
								  <div className="BlkIcn Home-Icon-Bg"><img src={require("./images/HM1-medical-query.png")} className="IgSz" /></div>
								  <div className="LnkTxtOtr" onClick={handleClick} >
									<div className="LnkHd Home-Link-Hd">Medical Query</div>
									<div className="LnkSub">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
								  </div>
								</a></div>
							  <div className="BlkLnkOtr Home-Links-Text"><a href="#">
								  <div className="BlkIcn Home-Icon-Bg"><img src={require("./images/HM2-medical-query.png")} className="IgSz" /></div>
								  <div className="LnkTxtOtr">
									<div className="LnkHd Home-Link-Hd">Find Doctors</div>
									<div className="LnkSub">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
								  </div>
								</a></div>
							  <div className="BlkLnkOtr Home-Links-Text"><a href="#">
								  <div className="BlkIcn Home-Icon-Bg"><img src={require("./images/HM3-medical-query.png")} className="IgSz" /></div>
								  <div className="LnkTxtOtr">
									<div className="LnkHd Home-Link-Hd">Search Doctor by Name</div>
									<div className="LnkSub">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
								  </div>
								</a></div>
							  <div className="BlkLnkOtr Home-Links-Text"><a href="#">
								  <div className="BlkIcn Home-Icon-Bg"><img src={require("./images/HM4-medical-query.png")} className="IgSz" /></div>
								  <div className="LnkTxtOtr">
									<div className="LnkHd Home-Link-Hd">Follow Up</div>
									<div className="LnkSub">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
								  </div>
								</a></div>
							</div>
						  </div>
						</div>
					  </div>
					  <div className="TxtBxOtr Bottm-Input-Box-Bg">
						<div className="SerBtn"><img src={require("./images/plus.png")} className="IgSz Pls" />
						  <div className="MnuOtr">
							<div style={{overflow: 'visible'}}>
							  <div className="MnuItmOtr" onClick={handleClick}>
								<div className="MnuItm"><img src={require("./images/01-medical-query.png")} className="IgSz" /></div>
								<div className="MnuItmTxt">Medical Query</div>
							  </div>
							  <div className="MnuItmOtr">
								<div className="MnuItm"><img src={require("./images/02-Search-Doctor.png")} className="IgSz" /></div>
								<div className="MnuItmTxt">Find Doctors</div>
							  </div>
							  <div className="MnuItmOtr">
								<div className="MnuItm"><img src={require("./images/03-Book-Appointment.png")} className="IgSz" /></div>
								<div className="MnuItmTxt">Book Appointment</div>
							  </div>
							</div>
							<div style={{overflow: 'visible'}}>
							  <div className="MnuItmOtr">
								<div className="MnuItm"><img src={require("./images/04-Health-Check.png")} className="IgSz" /></div>
								<div className="MnuItmTxt">Health Checkups</div>
							  </div>
							  <div className="MnuItmOtr">
								<div className="MnuItm"><img src={require("./images/05-Body-Check.png")} className="IgSz" /></div>
								<div className="MnuItmTxt">Whole Body Checkup</div>
							  </div>
							  <div className="MnuItmOtr">
								<div className="MnuItm"><img src={require("./images/06-Search.png")} className="IgSz" /></div>
								<div className="MnuItmTxt">Search Doctor</div>
							  </div>
							</div>
							<div style={{overflow: 'visible'}}>
							  <div className="MnuItmOtr">
								<div className="MnuItm"><img src={require("./images/07-Call-Back.png")} className="IgSz" /></div>
								<div className="MnuItmTxt">Request Callback</div>
							  </div>
							  <div className="MnuItmOtr">
								<div className="MnuItm"><img src={require("./images/08-Restart.png")} className="IgSz" /></div>
								<div className="MnuItmTxt">Restart Conversation</div>
							  </div>
							</div>
						  </div>
						</div>
						<input type="text" placeholder="How can I help you..." className="TxtBx Bottom-InPut-Box" />
						<div className="TxtBxBtn"><a href="#"><img src={require("./images/plane.png")} className="IgSz" border={0} /></a></div>
						<div className="Clr" />
					  </div>
					</div>
				  </div>
				  <div className="IcnAni AniBg-1" />
				  <div className="IcnAni-2 AniBg-2" />
				</div>
			  );
	 }

export default IconClick;
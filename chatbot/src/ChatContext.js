import { createContext } from "react";

// No need to create functional component, simply export context
const ChatWindow = createContext(false);

export default ChatWindow;

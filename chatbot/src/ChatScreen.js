import React, { Component, useState,useContext } from 'react';
import ChatWindow  from './ChatContext';
import './css/Chat.scss';
import './css/ChatTheme.scss';



const ChatScreen = () =>  {
  const [window, setWindow] = useContext(ChatWindow) ;
  


        function handleExit() {		
          console.log(window);	
          setWindow(!window);
      } 
      return (
                 <div className="IcnOtr">
                      <div className="IcnClk2">
                        <img src={require("./images/woman.png")} className="IgSz" style={{boxShadow: '0px 0px 12px #a4a4a4', borderRadius: '100%'}} />
                        <div className="ChtWndOtr Window-Bg">
                          <div className="TpHdr Header-Bg">
                            <div className="Hdr Header-Text-Color">Chat Bot</div>
                            <div className="MnuIcn" onClick={handleExit}> 
                              <img src={require("./images/close.png")} className="IgSz" />
                            </div>
                            <div className="Clr" />
                            <div className="BblOtr">
                              <div className="BblRf-1">
                                <div className="BblOutImg"><img src={require("./images/chat-1.png")} className="IgSz" /></div>
                                <div className="BblOut Chat-Out-Message">Hi </div>
                              </div>
                              <div className="Clr" />
                              <div className="BblRf-2">
                                <div className="BblInImg-2"><img src={require("./images/chat-2.png")} className="IgSz" /></div>
                                <div className="BblIn Chat-In-Message">Hi</div>
                              </div>
                              <div className="Clr" />
                              <div className="BblRf-1">
                                <div className="BblOutImg"><img src={require("./images/chat-1.png")} className="IgSz" /></div>
                                <div className="BblOut Chat-Out-Message">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                              </div>
                              <div className="Clr" />
                              <div className="BblRf-2">
                                <div className="BblInImg-2"><img src={require("./images/chat-2.png")} className="IgSz" /></div>
                                <div className="BblIn Chat-In-Message">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</div>
                              </div>
                              <div className="Clr" />
                              <div className="BblRf-1">
                                <div className="BblOutImg"><img src={require("./images/chat-1.png")} className="IgSz" /></div>
                                <div className="BblOut Chat-Out-Message">incididunt ut </div>
                              </div>
                              <div className="Clr" />
                              <div className="BblRf-2">
                                <div className="BblInImg-2"><img src={require("./images/chat-2.png")} className="IgSz" /></div>
                                <div className="BblIn Chat-In-Message">ullamco laboris nisi ut aliquip</div>
                              </div>
                              <div className="Clr" />
                            </div>
                          </div>
                          <div className="TxtBxOtr Bottm-Input-Box-Bg">
                            <div className="SerBtn"><img src={require("./images/plus.png")} className="IgSz Pls" />
                              <div className="MnuOtr">
                                <div style={{overflow: 'visible'}}>
                                  <div className="MnuItmOtr">
                                    <div className="MnuItm"><img src={require("./images/01-medical-query.png")} className="IgSz" /></div>
                                    <div className="MnuItmTxt">Medical Query</div>
                                  </div>
                                  <div className="MnuItmOtr">
                                    <div className="MnuItm"><img src={require("./images/02-Search-Doctor.png")} className="IgSz" /></div>
                                    <div className="MnuItmTxt">Find Doctors</div>
                                  </div>
                                  <div className="MnuItmOtr">
                                    <div className="MnuItm"><img src={require("./images/03-Book-Appointment.png")} className="IgSz" /></div>
                                    <div className="MnuItmTxt">Book Appointment</div>
                                  </div>
                                </div>
                                <div style={{overflow: 'visible'}}>
                                  <div className="MnuItmOtr">
                                    <div className="MnuItm"><img src={require("./images/04-Health-Check.png")} className="IgSz" /></div>
                                    <div className="MnuItmTxt">Health Checkups</div>
                                  </div>
                                  <div className="MnuItmOtr">
                                    <div className="MnuItm"><img src={require("./images/05-Body-Check.png")} className="IgSz" /></div>
                                    <div className="MnuItmTxt">Whole Body Checkup</div>
                                  </div>
                                  <div className="MnuItmOtr">
                                    <div className="MnuItm"><img src={require("./images/06-Search.png")} className="IgSz" /></div>
                                    <div className="MnuItmTxt">Search Doctor</div>
                                  </div>
                                </div>
                                <div style={{overflow: 'visible'}}>
                                  <div className="MnuItmOtr">
                                    <div className="MnuItm"><img src={require("./images/07-Call-Back.png")} className="IgSz" /></div>
                                    <div className="MnuItmTxt">Request Callback</div>
                                  </div>
                                  <div className="MnuItmOtr">
                                    <div className="MnuItm"><img src={require("./images/08-Restart.png")} className="IgSz" /></div>
                                    <div className="MnuItmTxt">Restart Conversation</div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <input type="text" placeholder="How can I help you..." className="TxtBx Bottom-InPut-Box" />
                            <div className="TxtBxBtn"><a href="#"><img src={require("./images/plane.png")} className="IgSz" border={0} /></a></div>
                            <div className="Clr" />
                          </div>
                        </div>
                      </div>
                      <div className="IcnAni AniBg-1" />
                      <div className="IcnAni-2 AniBg-2" />
                    </div>
                  );
                }
            

export default ChatScreen;

import React, {useContext, useState} from 'react';
import './App.css';
// import context from context-api file
import ChatWindow  from './ChatContext'
import ChatScreen from './ChatScreen';
import IconClick from './IconClick';

function App() {
  // set default state in App.js to false
  const testContext = useState(false);
  return (
    // set provider with value of default case created above
    <ChatWindow.Provider value={testContext}>
 
    <div className="App">
      {testContext[0]==true ? <ChatScreen/>: <IconClick/>
}
    </div>
         
    </ChatWindow.Provider>
  );
}

export default App;